import XCTest

import PL_NetworkingFrameworkTests

var tests = [XCTestCaseEntry]()
tests += PL_NetworkingFrameworkTests.allTests()
XCTMain(tests)
