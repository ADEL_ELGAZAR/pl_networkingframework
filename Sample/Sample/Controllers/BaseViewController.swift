//
//  BaseViewController.swift
//  ReusableComponents
//
//  Created by FAB LAB on 10/16/19.
//  Copyright © 2019 FAB LAB. All rights reserved.
//

import UIKit
import PL_NetworkingFramework
import Moya

class BaseViewController: UIViewController, PL_NetworkCallBack {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func didDataRetrieved(endPoint: PL_ApiService, response: Response) {
        switch endPoint {
        case is Service_TestUpload:
            do {
                let result = try response.map(to: Response_Upload.self)
                for item in result.data {
                    print(item)
                }
            } catch {
            }
        default:
            return
        }
    }

    func didServerFail(endPoint: PL_ApiService, responseModel: PL_NetworkResponseModel) {
        
    }

    func didNetworkFail(endPoint: PL_ApiService) {
        
    }

    func didMappingFail(endPoint: PL_ApiService) {
        
    }
}
