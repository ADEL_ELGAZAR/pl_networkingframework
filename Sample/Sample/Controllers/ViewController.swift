//
//  ViewController.swift
//  Sample
//
//  Created by FAB LAB on 11/10/19.
//  Copyright © 2019 FAB LAB. All rights reserved.
//

import UIKit
import PL_NetworkingFramework

class ViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let image = UIImage.init(named: "")
        // build request model
        let request_model = Request_TestUpload(name: "Adel", files: [PL_File(file: image!.pngData(), fileName: "file", mimeType: "")])

        // build service to be called
        let service = Service_TestUpload(domain: AppDelegate.publicURL, inputModel: request_model)

        // contact network manager to call service with a callback in BaseViewContoller
        PL_NetworkManager().send(endPoint: service, callBack:self)
    }


}

