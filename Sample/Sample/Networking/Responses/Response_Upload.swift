//
//  PL_NetworkResponse.swift
//  PL_NetworkingFramework
//
//  Created by FAB LAB on 10/14/19.
//  Copyright © 2019 FAB LAB. All rights reserved.
//

import Foundation
import PL_NetworkingFramework
import Mapper

public struct Response_Upload: Mappable {
    public var data:[String]
    
    public init(map: Mapper) {
        data = map.optionalFrom("data") ?? []
    }
}
