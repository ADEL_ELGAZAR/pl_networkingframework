//
//  Request_TestUpload.swift
//  ReusableComponents
//
//  Created by FAB LAB on 10/27/19.
//  Copyright © 2019 FAB LAB. All rights reserved.
//

import Foundation
import PL_NetworkingFramework

public struct Request_TestUpload {
    var name:String
    var files:[PL_File]
}
