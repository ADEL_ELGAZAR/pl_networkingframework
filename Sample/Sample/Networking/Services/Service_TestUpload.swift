//
//  File.swift
//  PL_OAuthFramework
//
//  Created by FAB LAB on 10/16/19.
//  Copyright © 2019 FAB LAB. All rights reserved.
//

import Foundation
import PL_NetworkingFramework
import Moya

public final class Service_TestUpload:PL_ApiService {
    
    public override var path: String {
        return "/api/Account/TestUploadMutiple"
    }

    public override var method: Moya.Method {
        return .post
    }
    
    public override var parameterEncoding: PL_ParameterEncoding {
        return .FormData(parameters: parameters)
    }
    
    public override var headers: [String : String]? {
        var header:[String:String] = [:]
        header["Authorization"] = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6IkdhbGFsIiwiZW1haWwiOiJBYm8uZ2FsYWwuOTc5MjRAZ21haWwuY29tIiwianRpIjoiODVkODU4YTQtZWIzNi00YTNmLWI5YzEtNWE3NTc0YTU3Y2Q4IiwiZXhwIjoxNTc0MjYxMTExLCJpc3MiOiJodHRwczovL2xvY2FsaG9zdDo0NDMzOS8iLCJhdWQiOiJodHRwczovL2xvY2FsaG9zdDo0NDMzOS8ifQ.4hUexAQRkZYHZjgGd-qyB4vv8HGpfeJU2c31zVK461Q"
        return header
    }
}
