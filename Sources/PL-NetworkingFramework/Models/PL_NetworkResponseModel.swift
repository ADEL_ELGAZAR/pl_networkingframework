//
//  PL_NetworkResponse.swift
//  PL_NetworkingFramework
//
//  Created by FAB LAB on 10/14/19.
//  Copyright © 2019 FAB LAB. All rights reserved.
//

import Foundation
import Mapper

public struct PL_NetworkResponseModel: Mappable {
    var statusCode : PL_NetworkStatusCode?
    var message : String
    var errors : [PL_ValidationErrorModel]

    public init(map: Mapper) {
        statusCode = PL_NetworkStatusCode(rawValue: map.optionalFrom("statusCode") ?? 200)
        message = map.optionalFrom("message") ?? ""
        errors = map.optionalFrom("errors") ?? []
    }
}
