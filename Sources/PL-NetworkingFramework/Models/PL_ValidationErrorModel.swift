//
//  PL_ValidationError.swift
//  PL_NetworkingFramework
//
//  Created by FAB LAB on 10/14/19.
//  Copyright © 2019 FAB LAB. All rights reserved.
//

import Foundation
import Mapper

struct PL_ValidationErrorModel : Mappable {
    var key : String
    var value : String
    
    init(map: Mapper) {
        key = map.optionalFrom("key") ?? ""
        value = map.optionalFrom("value") ?? ""
    }
}
