//
//  PL_ParameterEncoding.swift
//  PL_NetworkingFramework
//
//  Created by FAB LAB on 10/26/19.
//  Copyright © 2019 FAB LAB. All rights reserved.
//QueryString

import Foundation
import Moya
//import Alamofire

public enum PL_ParameterEncoding {
    
    case QueryString(parameters:[String:Any])
    case JsonEncoding(parameters:[String:Any])
    case FormData(parameters:[String:Any])
    
    public var task: Task {
        switch self {
        case .QueryString(let parameters):
            return .requestParameters(parameters: parameters, encoding: URLEncoding.queryString)
        case .JsonEncoding(let parameters):
            return .requestParameters(parameters: parameters, encoding: JSONEncoding.default)
        case .FormData(let parameters):
            var parts:[MultipartFormData] = []
            for parameter in parameters {
                if let file = (parameter.value as? PL_File) {
                    let part = MultipartFormData(provider: .data(file.file!), name: parameter.key, fileName: file.fileName, mimeType: file.mimeType)
                    parts.append(part)
                } else if let files = (parameter.value as? [PL_File]) {
                    for file in files {
                        let part = MultipartFormData(provider: .data(file.file!), name: parameter.key, fileName: file.fileName, mimeType: file.mimeType)
                        parts.append(part)
                    }
                } else {
                    let part = MultipartFormData(provider: .data("\(parameter.value)".data(using: .utf8)!), name: parameter.key)
                    parts.append(part)
                }
            }
            return .uploadMultipart(parts)
        }
    }
}
