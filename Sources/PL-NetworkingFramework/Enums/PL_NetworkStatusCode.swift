//
//  PL_NetworkStatusCode.swift
//  PL_NetworkingFramework
//
//  Created by FAB LAB on 10/14/19.
//  Copyright © 2019 FAB LAB. All rights reserved.
//

import Foundation

public enum PL_NetworkStatusCode:Int {
    case Success = 200
    case ValidationError = 400
    case ServerNotFound = 404
    case ServerError = 500
}
