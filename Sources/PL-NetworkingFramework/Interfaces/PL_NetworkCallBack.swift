//
//  PL_NetworkCallBack.swift
//  PL_NetworkingFramework
//
//  Created by FAB LAB on 10/14/19.
//  Copyright © 2019 FAB LAB. All rights reserved.
//

import Foundation
import Moya

public protocol PL_NetworkCallBack {
    func didDataRetrieved(endPoint: PL_ApiService, response:Response)
    func didServerFail(endPoint: PL_ApiService, responseModel: PL_NetworkResponseModel)
    func didNetworkFail(endPoint: PL_ApiService)
    func didMappingFail(endPoint: PL_ApiService)
}
