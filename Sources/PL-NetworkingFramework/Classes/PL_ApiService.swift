//
//  PL_ApiService.swift
//  PL_NetworkingFramework
//
//  Created by FAB LAB on 10/14/19.
//  Copyright © 2019 FAB LAB. All rights reserved.
//

import Foundation
import Moya

open class PL_ApiService:TargetType {
    let domain:String
    public let inputModel:Any

    public init(domain:String, inputModel:Any) {
        self.domain = domain
        self.inputModel = inputModel
    }
    
    open var baseURL: URL { return URL(string: domain.isEmpty ? "www.google.com" : domain)! }
    
    open var path: String {
        return ""
    }
    
    open var method: Moya.Method {
        return .post
    }
    
    public final var parameters: [String: Any] {
        var parameters = [String: Any]()
        let mirror = Mirror(reflecting: inputModel)
        for (name, value) in mirror.children {
            guard let name = name else { continue }
            print("\(name): \(type(of: value)) = '\(value)'")
            parameters[name] = value
        }
        return parameters
    }
    
    public final var task: Task {
        return parameterEncoding.task
    }
    
    open var parameterEncoding: PL_ParameterEncoding {
        return .QueryString(parameters: parameters)
    }
    
    open var headers: [String: String]? {
        return [String: String]()
    }
    
    open var sampleData: Data {
        return Data()
    }
}
