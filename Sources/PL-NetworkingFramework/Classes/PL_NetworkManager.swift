//
//  PL_NetworkRequest.swift
//  PL_NetworkingFramework
//
//  Created by FAB LAB on 10/14/19.
//  Copyright © 2019 FAB LAB. All rights reserved.
//

import Moya
import Mapper
import Moya_ModelMapper
import Reachability
import Async

public final class PL_NetworkManager {
    private var networkReachability:Reachability?
    public init() {
        do {
            try networkReachability = Reachability()
        } catch { }
    }
    
    public func send(endPoint service:PL_ApiService, callBack:PL_NetworkCallBack) {
        if networkReachability?.connection == .unavailable {
            // no internet connection
            callBack.didNetworkFail(endPoint: service)
        } else {
//            ARSLineProgress.show()
            let provider = MoyaProvider<PL_ApiService>()
            provider.request(service) { (result) in
//                ARSLineProgress.hide()
                Async.main {
                    switch result {
                    case .success(let response):
                        do {
                            let networkResponse = try response.map(to: PL_NetworkResponseModel.self)
                            switch networkResponse.statusCode {
                            case .Success:
                                callBack.didDataRetrieved(endPoint: service, response: response)
                            case .ValidationError, .ServerNotFound, .ServerError:
                                callBack.didServerFail(endPoint: service, responseModel: networkResponse)
                            case .none:
                                callBack.didMappingFail(endPoint: service)
                            }
                        } catch {
                            callBack.didMappingFail(endPoint: service)
                        }
                    case .failure(let error):
                        print("\(error)")
                        callBack.didNetworkFail(endPoint: service)
                    }
                }
            }
        }
    }
}
