//
//  PL_File.swift
//  PL_NetworkingFramework
//
//  Created by FAB LAB on 10/26/19.
//  Copyright © 2019 FAB LAB. All rights reserved.
//

import Foundation
import Mapper

public struct PL_File {
    public var file: Data?
    public var fileName: String
    public var mimeType:String
    
    public init(file: Data?, fileName: String, mimeType:String) {
        self.file = file
        self.fileName = fileName
        self.mimeType = mimeType
    }
    
    public init(map: Mapper) {
        fileName = map.optionalFrom("fileName") ?? ""
        mimeType = map.optionalFrom("mimeType") ?? ""
    }
}
